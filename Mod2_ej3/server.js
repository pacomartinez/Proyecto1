// una función para validación de campos y evitar inyecciones:
function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

// carga de librerías adicionales:
var express = require('express');
var app = express();
//cuando mande form por get o post saber analizarlos
var bodyParser = require('body-parser');
var mysql      = require('mysql');
var fs = require('fs');
var port = 3000;
var config = {};
var connection = null;

// leer datos de conexión a MySql de config.json, 
// para no distribuirlos con los fuentes
// y conectar
//fx arrow, version 6 de javascript, solo quita la palabra function
fs.readFile('./config.json', (err,data) => {
  config = JSON.parse(data);  
  connection = mysql.createConnection(config);
  connection.connect();
});

// configuración de /public y bodyParser
app.use( bodyParser.json() );  
app.use( bodyParser.urlencoded({ extended: true }) );  
app.use( express.static('public') );

// ruta BASE
// GET /
//estas lineas son más para educacion, por si alguien pide barra/
app.get('/', function (req, res) {
  //res.sendFile( __dirname + '/forms/index.html');  
res.sendFile( __dirname + '/indexej3.html');
});

// listado JSON de alumnos con SELECT
// GET /alumnos
app.get('/alumnos', function (req, res) {      
  // TODO: pasarle ?orderby=campo
  connection.query('SELECT * FROM alumnos;', function (error, results, fields) {        
    res.json(results);
  });  
});



// obtiene de MySql el alumno con id = :id
// GET /alumno/:id
app.get('/alumno/:id', (req,res) => {
  let sql = 'select * from alumnos where id = ?;'
  connection.query(sql, [req.params.id], function (error, results, fields) {     
    // si no se encuentra el registro se devuelve objeto blanco mejor que pantalla blanca
    res.json( results[0] ? results[0] : {} )
    //res.json(error )
  });    
})

// grabación de alumnos vía html FORM 
// a) los datos se envían con método POST en el cuerpo del request
// b) por convención, el INSERT o UPDATE se decide por el valor del
// campo id del FORM; puesto que es un campo hidden, sólo puede tener
// valor si se está editando y se hace UPDATE; si no tiene valor se hace INSERT
// POST /alumno --data {...}
app.post('/alumno', (req,res) => {     
  let alumno = req.body, sql = '' ;
  
  if ( parseInt(alumno.id) > 0 ){    
    sql = 'update alumnos set apellidos = ? , nombre = ? where id = ?';
    connection.query(sql, [alumno.apellidos, alumno.nombre, alumno.id], function (error, results, fields) {      
    });    
  } else {
    sql = 'insert into alumnos (apellidos,nombre) values (?, ?)';
    connection.query(sql, [alumno.apellidos, alumno.nombre], function (error, results, fields) {
      console.log(error,results)
    });   
  }
  res.redirect('/newalumno');
});

// borrado de alumnos:
// desde el cliente se manda un Ajax de type DELETE
// DELETE /alumno/:id
app.delete('/alumno/:id', (req,res) => {       
  let sql = 'delete from alumnos where id = ?'
  connection.query(sql, [req.params.id], function (error, results, fields) {
    res.json(results)
  });
});

// envío de archivos que no están en la carpeta de static
// por ejemplo formularios de edición, otros archivos de la app
// o cualquier archivo cuya url queramos no mostrar;
app.get('/newalumno', (req, res) => {
  res.sendFile( __dirname + '/indexej3.html');  
});

// arrancar el server
app.listen(port, function(){
  console.log('mysql middleWare escuchando en puerto ' + port);
});

// TODO:
// hacer archivo log de las peticiones importantes
// hacer validación de los campos que se graban


var DefaultAjax = {
	  type: "get",
	  url: "/alumnos",
	  data: null,
	  success: function(data){ console.log(data)},
	  failure: function(errMsg){ console.log(errMsg) },
	  dataType: 'json',
	  contentType: "application/json; charset=utf-8"
	};			

function getAlumnos() {

	DefaultAjax.type = 'get'
	DefaultAjax.url = "/alumnos"
	$.ajax(DefaultAjax)
	.done(function (data) {
	
		$('#selector').html('');

		$.each(data, function(idx,row){
			console.log(row);			
			let html=`
			<option value="${row.id}">
			${row.apellidos}, ${row.nombre} </option>`
			$('#selector').append(html)
		});
	});

}

function cambiaCombo(){	
	
	DefaultAjax.type = 'get'
	DefaultAjax.url = '/alumno/' + $('#selector option:selected').val()

	
	$.ajax(DefaultAjax).done(function(data){
		$('#id').val(data.id)
		$('#apellidos').val(data.apellidos)
		$('#nombre').val(data.nombre)


		})
	
}
//recoger los inputs del formulario en un objeto
function modificar(){
	var doc = {
		id: $('#id').val(),
		apellidos: $('#apellidos').val(),
		nombre: $('#nombre').val()
	}
	//enviar post a couchdb
	//otra opcion defaulAjax.success
	DefaultAjax.type = 'post'
	DefaultAjax.url = '/alumno'
	DefaultAjax.data = JSON.stringify(doc)
	$.ajax(DefaultAjax).done(function(data){
		
		console.log(data)
	})
	limpiar()
	getAlumnos()

	console.log(doc);

}

//borrar
function borrar(){

	var borrado = {
		id: $('#id').val(),
		
		}

	DefaultAjax.type = 'delete'
	DefaultAjax.url = '/alumno/' + $('#id').val()

	//DefaultAjax.data = JSON.stringify(borrado)

	$.ajax(DefaultAjax).done(function(data){
		console.log(data)
	})

	limpiar()
	getAlumnos()
}

function crear(){
	var creado = {
		apellidos: $('#apellidos').val(),
		nombre: $('#nombre').val()
	}

	DefaultAjax.type = 'post'
	DefaultAjax.url = '/alumno'
	DefaultAjax.data = JSON.stringify(creado)
	
	$.ajax(DefaultAjax).done(function(data){
		console.log(data)
	})
	limpiar()


	getAlumnos()
	
	console.log(creado);

}

function limpiar(){
	$('#apellidos').val(""),
	$('#nombre').val("")

}


function listadonombre(data){
	datos = JSON.parse(data)
	$('#result').html('')
	$.each(datos.row, function(idx,row){
		$('#result').append('<li>' + row.key + '</li>')
	});
	$('selectalumnos').hide()
	$('#result').show()


}






$(document).ready(function(){
	getAlumnos()

})




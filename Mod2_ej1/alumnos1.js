const database = 'http://192.168.100.71:5984/franciscada/'

var DefaultAjax = {
	  type: "get",
	  url: database + "_all_docs?include_docs=true",
	  data: null,
	  success: function(data){ console.log(data)},
	  failure: function(errMsg){ console.log(errMsg) },
	  dataType: 'json',
	  contentType: "application/json; charset=utf-8"
	};			

function getAlumnos() {
	DefaultAjax.type = 'get'
	DefaultAjax.url = database + "_all_docs?include_docs=true"
	$.ajax (DefaultAjax)
	.done(function (data) {
		console.log(data.rows)

		$('#selector').html('<option> </option>');

		$.each(data.rows, function(idx,row){
			console.log(row.doc._id, row.doc.apellidos, row.doc.nombre);
			
		let html=`
		<option value="${row.id}">
		${row.doc.apellidos}, ${row.doc.nombre} </option>`
		$('#selector').append(html)
		});
	});

}

function cambiaCombo(){
	$('#selector option:selected').val()
	DefaultAjax.type = 'get'
	DefaultAjax.url = database + $('#selector option:selected').val()

	/* otra alternativa
	DefaultAjax.success = function(){
	}
	*/
	$.ajax(DefaultAjax).done(function(data){
		$('#id').val(data._id)
		$('#rev').val(data._rev)
		$('#apellidos').val(data.apellidos)
		$('#nombre').val(data.nombre)


		})
	//alert('Hello')
}
//recoger los inputs del formulario en un objeto
function modificar(){
	var doc = {
		_id: $('#id').val(),
		_rev: $('#rev').val(),
		apellidos: $('#apellidos').val(),
		nombre: $('#nombre').val()
	}
	//enviar post a couchdb
	//otra opcion defaulAjax.success
	DefaultAjax.type = 'post'
	DefaultAjax.url = database 
	DefaultAjax.data = JSON.stringify(doc)
	$.ajax(DefaultAjax).done(function(data){
		
		console.log(data)
	})

	getAlumnos()

	console.log(doc);

}

//borrar
function borrar(){

	var borrado = {
		_id: $('#id').val(),
		_rev: $('#rev').val(),
		}

	DefaultAjax.type = 'delete'
	DefaultAjax.url = database + $('#id').val() + "?rev=" + $('#rev').val()
	//DefaultAjax.data = JSON.stringify(borrado)

	$.ajax(DefaultAjax).done(function(data){
		console.log(data)
	})
	limpiar()
	getAlumnos()
}

function crear(){
	var creado = {
		apellidos: $('#apellidos').val(),
		nombre: $('#nombre').val()
	}

	DefaultAjax.type = 'post'
	DefaultAjax.url = database
	DefaultAjax.data = JSON.stringify(creado)
	
	$.ajax(DefaultAjax).done(function(data){
		console.log(data)
	})
	limpiar()


	getAlumnos()
	
	console.log(creado);

}

function limpiar(){
	$('#apellidos').val(""),
	$('#nombre').val("")

}
//posible vista para integrar en el CRUD CouchDB
// function tempView(attr1,attr2,callback)	{
// 	let view = `{
// 		"map":"function(doc){if(doc.$attr1)&& doc.$(attr2){emit(doc.${attr1},doc.${attr2})}}"
// 	}`

// $.ajax({
// 	type: "post",
// 	success: callback,
// 	data: view,
// 	url: database + '_temp_view',
// 	contentType: "application/json; charset=utf-8"
// })

// }

function listadonombre(data){
	datos = JSON.parse(data)
	$('#result').html('')
	$.each(datos.row, function(idx,row){
		$('#result').append('<li>' + row.key + '</li>')
	});
	$('selectalumnos').hide()
	$('#result').show()


}






$(document).ready(function(){
	getAlumnos()

})



